class JobsController < ApplicationController

	def index
		@post=Job.search(params[:keyword]).filter(params[:filter]).paginate(page: params[:page], per_page: 6)
		@category=Category.all
		@post_count=Job.search(params[:keyword]).filter(params[:filter]).count
	end


	def contact
	end

	def show
		@post=Job.find(params[:id])
	end

	def new
		@post=Job.new
	end

	def category
		@category=Category.all
	end

	def compprofile
	end

	def create
		@post=Job.create(post_params)

		if @post.save
			redirect_to jobs_path
		else
			render new_job_path
		end
	end

	private
	def post_params
		params.require(:job).permit(:title,:category_id,:location,:description,:email_customer,:comp_name,:comp_url,:email_comp)

	end
end
