Rails.application.routes.draw do
  devise_for :users
  resources :jobs
  root to: 'jobs#index'
  get 'category', to: 'jobs#category', as: 'category'
  get 'contact', to: 'jobs#contact', as: 'contact'
  get 'compprofile', to: 'jobs#compprofile', as: 'compprofile'
  resources :contacts , only: [:new,:create]
end
